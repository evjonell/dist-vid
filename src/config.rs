use std::path::PathBuf;

use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "command")]
pub enum Command {
    Process,
    Retrieve
}

#[derive(StructOpt, Debug)]
#[structopt(name = "dist-vid")]
pub struct Config {
    #[structopt(flatten)]
    pub command: Command,
    
    #[structopt(short = "f", long)]
    pub filename: PathBuf,

    #[structopt(short = "m", long, default_value = "./manifests")]
    pub manifestdir: PathBuf,

    #[structopt(short = "h", long, default_value = "./hashes")]
    pub hashdir: PathBuf,

    #[structopt(short = "r", long, default_value = "./retrieved")]
    pub retrievedir: PathBuf,
}

impl Config {
    pub fn new() -> Self {
        Config::from_args()
    }
}

