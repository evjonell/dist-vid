use std::{
    fs,
    io::{BufReader, Read, Write},
    process,
};

use manifest::Manifest;
use sha2::{
    digest::generic_array::{typenum::U32, GenericArray},
    Digest, Sha256,
};

mod config;
mod manifest;

use config::{Command, Config};

/// make our chunks 16k in size
const CHUNK_SIZE: usize = 2_usize.pow(14);

fn main() {
    let config = Config::new();
    make_hash_dir(&config);
    make_manifest_dir(&config);
    make_retrieve_dir(&config);

    match config.command {
        Command::Process => process_file(&config),
        Command::Retrieve => retrieve_file(&config),
    }
}

/// retrieve a file made of chunks
fn retrieve_file(config: &Config) {
    // read in the manifest
    let manifest = Manifest::read(config);

    // open the destination file
    let filename = config.retrievedir.as_path().join(manifest.name);
    let mut retrieved_file = fs::File::create(filename).unwrap_or_else(|err| {
        eprintln!("encountered error creating destination file:\n{}", err);
        process::exit(1);
    });

    // for every chunk in our manifest:
    // - open the file
    // - read that chunk's contents
    // - write it into the destination file
    for chunk in manifest.chunks.iter() {
        let chunkname = config.hashdir.as_path().join(chunk);

        let data_file = fs::File::open(chunkname).unwrap_or_else(|err| {
            eprintln!("encountered error opening chunk file:\n{}", err);
            process::exit(1);
        });

        let mut reader = BufReader::new(data_file);
        // create a buffer of `CHUNK_SIZE` bytes
        let mut buff = [0_u8; CHUNK_SIZE];
        // read from the buffered readyer and capture how many bytes were
        // actually able to be read
        let size = match reader.read(&mut buff) {
            Ok(n) => n,
            Err(err) => {
                eprintln!("encountered error reading chunk file:\n{err}");
                process::exit(1);
            }
        };

        // write all of the given buffer up to `size` bytes
        if retrieved_file.write_all(&buff[0..size]).is_ok() {
            continue;
        } else {
            eprintln!("failed to write chunk {chunk}");
            process::exit(1);
        }
    }

    // flush our data to finalize all the writes
    if retrieved_file.flush().is_err() {
        eprintln!("failed to flush data to retrieved file");
    };
}

/// process a file into chunks
fn process_file(config: &Config) {
    let mut file = fs::File::open(&config.filename).unwrap_or_else(|err| {
        eprintln!("encountered error opening file:\n{}", err);
        process::exit(1);
    });

    // convert the configurations `PathBuf` to a manifest usable `String`
    let name = config
        .filename
        .file_name()
        .unwrap()
        .to_str()
        .unwrap()
        .to_string();

    let mut manifest = Manifest {
        name,
        chunks: vec![],
    };

    // read in up to `CHUNK_SIZE` bytes from the source file saving each chunk
    loop {
        let mut buff = [0_u8; CHUNK_SIZE];
        if let Ok(n) = file.read(&mut buff) {
            if n > 0 {
                let hash = hash_chunk(&buff);
                let hex_value = hex::encode(hash);
                write_chunk(config, &buff, &hex_value, &n);
                manifest.chunks.push(hex_value.clone());
            } else {
                break;
            }
        } else {
            break;
        }
    }

    // save our manifest
    manifest.write();
}

fn make_hash_dir(config: &Config) {
    if fs::create_dir_all(&config.hashdir).is_err() {
        eprintln!("failed to create hashes directory");
        process::exit(1);
    };
}

fn make_manifest_dir(config: &Config) {
    if fs::create_dir_all(&config.manifestdir).is_err() {
        eprintln!("failed to create manifests directory");
        process::exit(1);
    };
}

fn make_retrieve_dir(config: &Config) {
    if fs::create_dir_all(&config.retrievedir).is_err() {
        eprintln!("failed to create retrievedir directory");
        process::exit(1);
    };
}

fn hash_chunk(chunk: &[u8; CHUNK_SIZE]) -> GenericArray<u8, U32> {
    let mut hasher = Sha256::new();
    hasher.update(chunk);
    hasher.finalize()
}

/// write up to `size` bytes of `chunk` to the `config`'s `hashdir` location,
/// with the given `hash` as the file name
fn write_chunk(config: &Config, chunk: &[u8; CHUNK_SIZE], hash: &String, size: &usize) {
    let hashfile = config.hashdir.join(hash);
    if let Ok(mut file) = fs::File::create(hashfile) {
        // only write up to as much as was actually read
        if let Err(err) = file.write(&chunk[0..*size]) {
            eprintln!("failed to write file: \n{err}");
            process::exit(1);
        }
    } else {
        eprintln!("failed to open file for writing");
        process::exit(1);
    }
}
