use std::{fs, process};

use serde::{Deserialize, Serialize};

use crate::config::Config;

#[derive(Debug, Serialize, Deserialize)]
pub struct Manifest {
    pub name: String,
    pub chunks: Vec<String>,
}

impl Default for Manifest {
    fn default() -> Self {
        Self {
            name: "".to_string(),
            chunks: vec![],
        }
    }
}

impl Manifest {
    pub fn read(config: &Config) -> Manifest {
        let data = fs::read_to_string(&config.filename).unwrap_or_else(|err| {
            eprintln!("error reading manifest:\n {err}");
            process::exit(1);
        });

        let manifest: Manifest = serde_json::from_str(&data).unwrap_or_else(|err| {
            eprintln!("error deserializing manifest:\n {err}");
            process::exit(1);
        });

        manifest
    }

    pub fn write(&self) {
        let data = serde_json::to_string(self).unwrap_or_else(|err| {
            eprintln!("error serializing manifest:\n {err}");
            process::exit(1);
        });

        if let Err(err) = fs::write(String::from("./manifests/") + &self.name + &String::from(".json"), data) {
            eprintln!("error writing manifest:\n {err}");
            process::exit(1);
        };
    }
}
